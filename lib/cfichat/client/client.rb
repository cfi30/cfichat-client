module CfiChat
  class Client
    OK   = :green
    INFO = :blue
    WARN = :yellow
    ERR  = :red

    attr_reader :server_url, :user_list
    attr_accessor :connection_opts, :user_agent, :cookies
    attr_accessor :login_token, :room_password

    def initialize(url, version = nil, sum = nil)
      @server_url = url
      @server_version, @server_sum = version, sum

      @connection_opts = Hash.new
      @user_agent = "#{self.class}/#{VERSION}"
      @cookies = @login_token = @room_password = nil

      @login_hooks = Array.new
      @message_hooks = Array.new
      @status_hooks = Array.new

      @speak_queue = EM::Queue.new
      pop_queue = proc {|message|
        fetch_or_post message

        EM.add_timer 1 do
          @speak_queue.pop &pop_queue
        end
      }
      @speak_queue.pop &pop_queue

      @session = {
        :last_id   => nil,
        :locked_in => false,
        :overview  => nil,
        :sak       => nil,
        :time      => nil,
        :topic_crc => 0,
        :ul_crc    => 0,
        :user      => nil,
      }
      @user_list = UserList.new
    end

    def add_login_hook(&block)
      @login_hooks << block
    end

    def add_message_hook(&block)
      @message_hooks << block
    end

    def add_status_hook(&block)
      @status_hooks << block
    end

    def status(color, message)
      @status_hooks.each {|hook| hook[message.to_s, color] }
    end

    def run(first_room, update_delay = 8)
      @login_hooks.clear
      @message_hooks.clear
      @status_hooks.clear

      add_login_hook {
        EM.add_periodic_timer update_delay do
          fetch_or_post
        end
      }

      add_status_hook {|message, color|
        puts "#{Paint['::', color, :bold]} #{Paint[message, :bold]}"
      }

      EM.run {
        EM.add_shutdown_hook { puts; status OK, "Exiting" }
        yield if block_given?

        login first_room
      }
    rescue ClientError => e
      status ERR, "#{e.class}: #{e.message}"
    rescue Interrupt
    end

    def login(room_id)
      req = send_request({
        :login => @login_token,
        :rid   => room_id,
        :cver  => @server_version,
        :csum  => @server_sum,
      })
      req.callback {
        check_response :login, req or raise ClientError, "Login failed"
        resp = req.response

        @session[:last_id] = resp['last_id'].to_s
        @session[:sak] = resp['sak'].to_s
        @session[:time] = resp['time'].to_s
        @session[:user] = resp['user'].to_s

        status INFO, "Logged in as #{Paint[@session[:user], '#' + resp['color'].to_s]}"
        Message.parse(resp['welcome']) {|msg|
          @message_hooks.each {|hook| hook[msg] }
        }

        @login_hooks.each {|hook| hook[] }
      }
      req.errback { raise ClientError, req.error }
    end

    def speak(message)
      @speak_queue.push message unless message.empty?
    end

    def update; fetch_or_post; end

    protected
    def fetch_or_post(message = nil)
      req = send_request({
        :msg => message,
        :csum => @server_sum,
        :cver => @server_version,
        :last_id => @session[:last_id],
        :overview => @session[:overview],
        :password => @room_password,
        :sak => @session[:sak],
        :time => @session[:time],
        :topic_crc => @session[:topic_crc],
        :ul_crc => @session[:ul_crc],
        :user => @session[:user],
      })
      req.callback {
        check_response :update, req or next
        resp = req.response

        unless resp['erreur'].to_s.empty?
          raise ServerError, resp['erreur']
        end

        unless resp['popup'].to_s.empty?
          status WARN, resp['popup']
        end

        unless resp['adminWarning'].to_s.empty?
          status WARN, "[WARNING] #{resp['adminWarning'].strip}"
        end

        if resp['invited'].is_a?(Hash) && !resp['invited']['to'].to_s.empty?
          status INFO, "You have been invited in room #{resp['invited']['to']} (#{resp['invited']['name']}) by #{resp['invited']['by']}"
        end

        if resp['lockedIn'] != @session[:locked_in]
          if resp['lockedIn']
            status WARN, "You have been locked in this room -- /join cannot be used"
          else
            status OK, "You have been released"
          end
        end

        if resp['topicHash'] && resp['topicHash'] != @session[:topic_crc]
          status INFO, "Topic: #{Message.new(resp['topic']).text}"
        end

        @session[:last_id] = resp['last_id'] unless resp['last_id'] == 0
        @session[:locked_in] = resp['lockedIn']
        @session[:topic_crc] = resp['topicHash']
        @session[:ul_crc] = resp['ul_crc']
        @user_list.load resp['userList'] if resp['userList'].is_a? Hash

        unless resp['messages'].to_s.empty?
          Message.parse(resp['messages'], resp['blinkCount'].to_i) {|msg|
            @message_hooks.each {|hook| hook[msg] }
          }
        end
      }
      req.errback { status WARN, req.error }
    end

    def send_request(body)
      client = EM::HttpRequest.new(@server_url, @connection_opts)
      client.use EM::Middleware::JSONResponse

      client.post({
        :head => {
          'cookie' => @cookies,
          'user-agent' => @user_agent,
          'x-requested-with' => self.class,
        },
        :body => body,
      })
    end

    def check_response(type, req, &callback)
      status_code = req.response_header.status
      response = req.response

      if status_code != 200
        status WARN, "Server replied with a #{status_code} status code"
        return false
      end

      unless response.is_a?(Hash)
        raise ServerError, response[2..-1] if response.to_s.start_with? '0:'

        status WARN, "Invalid response received (not a JSON hash)"
        return false
      end

      case type
      when :login
        required_keys = [
          'color',
          'last_id',
          'sak',
          'time',
          'user',
          'welcome'
        ]
      when :update
        required_keys = [
          'adminWarning',
          'blinkCount',
          'erreur',
          'invited',
          'last_id',
          'lockedIn',
          'messages',
          'popup',
          'topic',
          'topicHash',
          'ul_crc',
          'userList',
        ]
      end

      missing_keys = required_keys - response.keys
      if missing_keys.length > 0
        status WARN, "Invalid response received (missing keys: #{missing_keys.inspect})"
        return false
      end

      return true
    end

    class ClientError < RuntimeError; end
    class ServerError < ClientError; end
  end
end
