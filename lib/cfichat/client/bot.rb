module CfiChat
  class Client::Bot
    def initialize(client)
      @client = client
      @client.status Client::OK, 'Bot enabled'
      @client.add_message_hook {|msg| execute msg }
    end

    def execute(msg)
      return unless msg.new?

      args = msg.body.split
      command = args.shift
      method_sym = "cmd_#{command}"

      reply_block = proc {|reply|
        if msg.query?
          @client.speak "#{msg.author}> #{reply}"
        else
          @client.speak "/pm #{msg.author}> #{reply}"
        end
      }

      @message = msg

      Fiber.new {
        if respond_to? method_sym
          send method_sym, *args, &reply_block
        else
          invalid_command command, *args, &reply_block
        end
      }.resume
    end

    def invalid_command(name, *received_args)
      # do nothing by default
    end
  end
end
