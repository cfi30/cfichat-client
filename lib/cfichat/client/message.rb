module CfiChat
  class Client::Message
    attr_reader :text, :to_html, :color
    attr_reader :time, :author, :receivers, :body
    def query?; @is_query; end
    def private?; @is_private; end
    def new?; @is_new; end

    def self.parse(raw_html, blink_count = 0)
      return to_enum(__method__, raw_html, blink_count) unless block_given?

      doc = Nokogiri::HTML raw_html
      node_set = doc.at_css('body > p') || doc.at_css('body')
      return unless node_set

      node_set.children.slice_before {|node|
        node.name == 'br'
      }.each {|chunk|
        chunk.shift if chunk.first.name == 'br'
        next if chunk.empty?

        node_set = Nokogiri::XML::NodeSet.new doc, chunk
        message = self.new node_set.to_html, blink_count > 0
        blink_count -= 1 if message.new?

        yield message
      }
    end

    def initialize(part_html, can_be_new = false)
      doc = Nokogiri::HTML part_html

      # line breaks
      doc.xpath('//text()').each {|node| node.unlink if node.text == "\n" }
      doc.css('br').each {|node| node.content = "\n" }

      # smileys
      doc.css('img[title]').each {|node| node.content = "\x20#{node['title']}\x20" }

      # codes
      doc.css('.linenodiv, .c_codetitle a').each {|node| node.unlink }

      # quotes
      doc.css('.c_quotetitle a').each {|node| node.unlink }
      doc.css('.c_quotecontent').each {|node| node.content = node.text.gsub(/\n\s/, "\n") }

      # links
      doc.css('a:not(.user)').each {|node|
        if node.text.empty? || ellipsed?(node.text, node['href'])
          node.replace "#{node['href']}"
        elsif node['href'] != node.text
          node.replace "#{node.text} (#{node['href']})"
        end
      }

      # message color
      html_color = nil
      %w[span i b].each {|tag|
        color_node = doc.at_css("#{tag}[style]")
        html_color = color_node ? color_node.styles['color'] : nil
        break if html_color
      }

      @to_html = part_html
      @text = doc.text.strip
      @color = html_color || :default
      @is_query = !doc.at_css('.new-msg').nil?
      @is_private = !doc.at_css('i[title]').nil?
      @is_new = can_be_new && (@is_query || !doc.at_css('.new-pm').nil?)

      parts = @text.match(/
        ^
        (?<time>\S+)
        \s+
        (?<author>[^\s:]+):
        \s+
        (?<receivers>([^\s>]+>\s+)*)
        (?<body>.+)
        $
      /mx)

      if parts
        @time = parts[:time]
        @author = parts[:author]
        @receivers = parts[:receivers].split />\s+/
        @body = parts[:body]
      else
        @receivers = Array.new
        @body = @text
      end
    end

    def to_s
      receivers = @receivers.join '> '
      receivers << '> ' unless receivers.empty?

      main = @author ? "#{@author}: #{receivers + @body}" : @body
      formatted = "#{@time} #{Paint[main, dup_color]}".strip

      if @is_query
        Paint[formatted, :bold]
      elsif @is_private
        Paint[formatted, :underline]
      else
        formatted
      end
    end

    private
    def ellipsed?(short, long)
      parts = short.split '…'
      parts.count == 2 &&
        long.start_with?(parts.first) && long.end_with?(parts.last)
    end

    def dup_color
      col = color
      col.is_a?(String) ? col.dup : col
    end
  end
end
