module CfiChat
  class Client::Input < EM::Connection
    include EM::Protocols::LineText2

    def initialize(client)
      @client = client
      @paste_mode = false
      @paste_buffer = Array.new

      @commands = [
        ["/clear", "Clear screen"],
        ["/exit!", "Force exit"],
        ["/help",  "Show local commands"],
        ["/names", "Show user list"],
        ["/pass",  "Set room password"],
        ["/paste", "Paste mode"],
      ]
    end

    def post_init
      @client.status Client::OK, 'Input enabled'
    end

    def receive_line(line)
      if @paste_mode
        @paste_buffer << line

        last_lines = @paste_buffer.last(2).uniq
        return unless last_lines.length == 1 && last_lines.first.empty?

        line = @paste_buffer[0..-3].join("\n")
        @paste_mode = false
        @paste_buffer.clear
        @client.status Client::OK, "Paste Mode: Disabled"
      end

      line.sub! /^.*[\e]/m, ''

      if @commands.assoc(line).nil?
        @client.speak line
      else
        send "cmd_#{line[1..-1]}"
      end
    end

    protected
    def cmd_clear
      print "\e[H\e[2J"
      @client.status Client::OK, "Screen cleared"
    end

    def cmd_exit!
      EM.stop
    end

    def cmd_pass
      @client.status Client::INFO, "Waiting for your password..."
      @client.room_password = STDIN.noecho(&:gets).chomp
      @client.status Client::OK, "Done"
    end

    def cmd_paste
      @paste_mode = true
      @client.status Client::INFO, "Paste Mode: Enabled - Type two empty lines to disable"
    end

    def cmd_names
      list = @client.user_list
      if list.empty?
        @client.status Client::ERR, "User list is not available at the moment"
        return
      end

      list.each {|room|
        label = "Room: #{room.name} (#{room.count})"
        label << " @" if room.op?
        label << " [Muted]" if room.muted?
        label << " [Private]" if room.private?
        @client.status Client::INFO, label

        users = Array.new
        room.each {|user|
          user_color = user.color == '#000000' ? :default : user.color
          formatting = [user.name, user_color]
          formatting << :underline if user.voiced?
          formatting << :inverse if user.sleeping?
          users << Paint[*formatting]
        }
        puts users.join ', ' unless users.empty?
      }
      @client.status Client::OK, "End of List"
    end

    def cmd_help
      @client.status Client::INFO, "#{self.class} command list:"
      @commands.each {|c|
        @client.status Client::INFO, "#{c[0]}\t#{c[1]}"
      }

      link = URI(@client.server_url)
      link.query = 'action=help'
      @client.status Client::OK, "See #{link} for the complete user guide and rules."
    end
  end
end
