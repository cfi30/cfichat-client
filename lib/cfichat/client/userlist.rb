module CfiChat
  class Client::UserList
    include Enumerable

    def initialize
      @rooms = Array.new
    end

    def load(list)
      return false unless list['rooms'].is_a? Array

      @rooms.clear
      list['rooms'].each {|room| @rooms << Room.new(room) }
    end

    def find(nick)
      each {|room|
        room.each {|user|
          return user if nick == user.name
        }
      }
      nil
    end

    def empty?; @rooms.empty?; end
    def each; @rooms.each {|r| yield r }; end

    class Room
      include Enumerable

      attr_reader :id, :name, :topic, :forum_url
      def allowed?; @is_allowed; end
      def muted?;   @is_muted;   end
      def private?; @is_private; end
      def op?;      @is_op;      end

      def initialize(r)
        @id = r['rid'].to_i
        @name = r['label'].to_s
        @topic = r['topic'].to_s
        @forum_url = r['forumUrl'].to_s
        @is_allowed = r['allowed'] == 1
        @is_muted = r['muted'] == 1
        @is_private = r['isprivate'] == 1
        @is_op = r['isAdmin'] == 1

        @users = Array.new
        if r['users'].is_a?(Array)
          r['users'].each {|user| @users << User.new(user) }
        end
      end

      def empty?; @users.empty?; end
      def each; @users.each {|r| yield r }; end
    end

    class User
      attr_reader :id, :name, :away, :color
      attr_reader :bullet, :group_id, :group_title
      def sleeping?; @is_sleeping; end
      def voiced?;    @has_voice;   end

      def initialize(u)
        @id = u['uid'].to_i
        @name = u['name'].to_s
        @away = u['status'].to_s
        @color = '#' + u['color'].to_s
        @bullet = u['bullet'].to_s
        @group_id = u['group'].to_i
        @group_title = u['usertitle'].to_s
        @is_sleeping = u['sleeping'] == 1
        @has_voice = u['hasVoice'] == true
      end
    end
  end
end
