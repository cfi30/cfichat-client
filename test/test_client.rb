require File.expand_path('../helper', __FILE__)

class TestClient < MiniTest::Test
  def test_version
    assert_equal "1", CfiChat::Client::VERSION
  end

  def test_status_consts
    assert_equal :green, CfiChat::Client::OK
    assert_equal :blue, CfiChat::Client::INFO
    assert_equal :yellow, CfiChat::Client::WARN
    assert_equal :red, CfiChat::Client::ERR
  end

  def test_set_server_url
    client = CfiChat::Client.new 'foo bar'
    assert_equal 'foo bar', client.server_url
  end

  def test_set_connection_opts
    opts = {:keepalive => true}
    client = CfiChat::Client.new nil

    assert_equal Hash.new, client.connection_opts
    client.connection_opts = opts
    assert_equal opts, client.connection_opts
  end

  def test_set_user_agent
    client = CfiChat::Client.new nil

    assert_equal 'CfiChat::Client/1', client.user_agent
    client.user_agent = 'foo bar'
    assert_equal 'foo bar', client.user_agent
  end

  def test_set_cookies
    cookies = 'name=value; eman=eulav'
    client = CfiChat::Client.new nil

    assert_nil client.cookies
    client.cookies = cookies
    assert_equal cookies, client.cookies
  end

  def test_set_login_token
    client = CfiChat::Client.new nil

    assert_nil client.login_token
    client.login_token = 'foo bar'
    assert_equal 'foo bar', client.login_token
  end

  def test_set_password
    client = CfiChat::Client.new nil

    assert_nil client.room_password
    client.room_password = 'foo bar'
    assert_equal 'foo bar', client.room_password
  end

  def test_status
    client = CfiChat::Client.new nil
    client.status CfiChat::Client::OK, 'foo bar'

    client.add_status_hook {|m, c| puts "s=#{m}, c=#{c}"}
    assert_output "s=foo bar, c=blue\n" do
      client.status :blue, 'foo bar'
    end

    client.add_status_hook {|m, c| puts "foo is #{m}, bar is #{c}"}
    assert_output "s=bar, c=foo\nfoo is bar, bar is foo\n" do
      client.status :foo, 'bar'
    end
  end

  def test_run
    stub_request(:post, "http://www.server.url/")
      .with(:body => "login=&rid=42&cver=&csum=")
      .to_return(:body => '{"color":"C0FF33", "last_id":1234, "sak":"qwfpgjluy", "time":4321, "user":"Foobar", "welcome":"hello world"}')

    client = CfiChat::Client.new 'http://www.server.url'
    client.add_login_hook { puts 'login hooks not cleared' }
    client.add_message_hook { puts 'message hooks not cleared' }
    client.add_status_hook { puts 'status hooks not cleared' }

    output = "#{Paint['::', :blue, :bold]} #{Paint['Logged in as ' + Paint['Foobar', '#C0FF33'], :bold]}\n"
    output << "#{Paint['::', :yellow, :bold]} #{Paint['Invalid response received (not a JSON hash)', :bold]}\n"
    output << "\n"
    output << "#{Paint['::', :green, :bold]} #{Paint['Exiting', :bold]}\n"
    output << "#{Paint['::', :red, :bold]} #{Paint['CfiChat::Client::ServerError: test error', :bold]}\n"

    assert_output output do
      client.run 42, 0.1 do
        client.add_login_hook {
          stub_request(:post, "http://www.server.url/")
            .with(:body => "msg=&csum=&cver=&last_id=1234&overview=&password=&sak=qwfpgjluy&time=4321&topic_crc=0&ul_crc=0&user=Foobar")
        }

        EM.add_timer(0.2) { raise CfiChat::Client::ServerError, "test error" }
      end
    end

    assert_requested :post, "http://www.server.url/", :body => "login=&rid=42&cver=&csum="
    assert_requested :post, "http://www.server.url/", :body => "msg=&csum=&cver=&last_id=1234&overview=&password=&sak=qwfpgjluy&time=4321&topic_crc=0&ul_crc=0&user=Foobar"
  end

  def test_run_interrupt
    client = CfiChat::Client.new nil
    capture_io {
      client.run 42 do
        raise Interrupt
      end
    }
  end

  def test_login_and_update
    client = CfiChat::Client.new 'http://www.server.url'
    EM.run {
      stub_request(:post, "http://www.server.url/")
        .with(:body => "login=&rid=42&cver=&csum=")
        .to_return(:body => '{"color":"C0FF33", "last_id":1234, "sak":"qwfpgjluy", "time":4321, "user":"Foobar", "welcome":""}')

      client.login 42
      EM.stop
    }

    assert_requested :post, "http://www.server.url/", :body => "login=&rid=42&cver=&csum="

    EM.run {
      stub_request(:post, "http://www.server.url/")
        .with(:body => "msg=&csum=&cver=&last_id=1234&overview=&password=&sak=qwfpgjluy&time=4321&topic_crc=0&ul_crc=0&user=Foobar")

      client.update
      EM.stop
    }

    assert_requested :post, "http://www.server.url/", :body => "msg=&csum=&cver=&last_id=1234&overview=&password=&sak=qwfpgjluy&time=4321&topic_crc=0&ul_crc=0&user=Foobar"
  end

  def test_speak_is_queued
    stub_request(:post, "http://www.server.url/")
      .with(:body => "msg=this%20will%20be%20sent%20now&csum=123123&cver=1.2.3&last_id=&overview=&password=&sak=&time=&topic_crc=0&ul_crc=0&user=")

    EM.run {
      client = CfiChat::Client.new 'http://www.server.url', '1.2.3', 123123
      client.speak '' # ignored
      client.speak 'this will be sent now'
      client.speak 'this will be sent one second later'
      client.speak 'this will be sent two seconds later'

      EM.stop
    }

    assert_requested :post, "http://www.server.url/"
  end

  def test_userlist
    client = CfiChat::Client.new 'http://www.server.url'
    assert_kind_of CfiChat::Client::UserList, client.user_list
  end
end
