require File.expand_path('../helper', __FILE__)

class TestMessage < MiniTest::Test
  def test_parse
    msgs = CfiChat::Client::Message.parse('message 1<br />message 2<br />').to_a
    assert_equal 2, msgs.length
    assert_equal 'message 1', msgs.first.to_html
    assert_equal 'message 2', msgs.last.to_html
  end

  def test_parse_yield
    msgs = []
    CfiChat::Client::Message.parse('message 1<br />message 2') {|m|
      msgs << m
    }
    assert_equal 2, msgs.length
  end

  def test_parse_multiline
    msgs = CfiChat::Client::Message.parse '<span>message 1<br />message 1</span>'
    assert_equal 1, msgs.count
  end

  def test_parse_empty
    msgs = CfiChat::Client::Message.parse String.new
    assert_equal 0, msgs.count
  end

  def test_html
    html = '<html><body><p>Hello<span>World</span></p></body></html>'
    msg = CfiChat::Client::Message.new html

    assert_equal html, msg.to_html
    assert_equal 'HelloWorld', msg.text
    assert_equal :default, msg.color

    refute msg.query?
    refute msg.private?
    refute msg.new?

    assert_nil msg.time
    assert_nil msg.author
    assert_empty msg.receivers
    assert_equal msg.text, msg.body
  end

  def test_link_simple
    html = 'a<a href="b">b</a>c'
    msg = CfiChat::Client::Message.new html
    assert_equal 'abc', msg.text
  end

  def test_link_label
    html = 'a<a href="/url">b</a>c'
    msg = CfiChat::Client::Message.new html
    assert_equal 'ab (/url)c', msg.text
  end

  def test_link_user
    html = 'a<a class="user" href="#">b</a>c'
    msg = CfiChat::Client::Message.new html
    assert_equal 'abc', msg.text
  end

  def test_link_ellipsed
    html = 'a<a href="http://mylongurl.com">http://mylo…rl.com</a>c'
    msg = CfiChat::Client::Message.new html
    assert_equal 'ahttp://mylongurl.comc', msg.text
  end

  def test_smiley
    html = 'a<img title="b"/>c'
    msg = CfiChat::Client::Message.new html
    assert_equal 'a b c', msg.text
  end

  def test_link_smiley
    html = '<span>a<a href="/url"><img title="b"/></a>c</span>'
    msg = CfiChat::Client::Message.new html
    assert_equal 'a b  (/url)c', msg.text
  end

  def test_image
    html = 'a<a href="/url"><img src="/"/></a>c'
    msg = CfiChat::Client::Message.new html
    assert_equal 'a/urlc', msg.text
  end

  def test_code
    html = "<div class=\"c_codetitle\">co<a>hello</a></div><div class=\"linenodiv\">line number</div>\n<pre>    de\nworld</pre>"
    msg = CfiChat::Client::Message.new html
    assert_equal "co    de\nworld", msg.text
  end

  def test_quote
    html = '<div class="c_quotetitle">h<a>fail</a></div><div class="c_quotecontent">ello<br /> world</div>'
    msg = CfiChat::Client::Message.new html
    assert_equal "hello\nworld", msg.text
  end

  def test_spaces
    html = '    foo    bar    '
    msg = CfiChat::Client::Message.new html
    assert_equal 'foo    bar', msg.text
  end

  def test_linebreak
    html = "foo<br /><span>bar</span>\n<span>baz</span>"
    msg = CfiChat::Client::Message.new html
    assert_equal "foo\nbarbaz", msg.text
  end

  def test_detect_color_span
    html = '<span style="color: blue"></span>'
    msg = CfiChat::Client::Message.new html
    assert_equal 'blue', msg.color
  end

  def test_detect_color_i
    html = '<i style="color: #C0FFEE;"></i>'
    msg = CfiChat::Client::Message.new html
    assert_equal '#C0FFEE', msg.color
  end

  def test_detect_color_b
    html = '<b style="color: #C0FFEE;"></b>'
    msg = CfiChat::Client::Message.new html
    assert_equal '#C0FFEE', msg.color
  end

  def test_detect_color_other
    html = '<u style="color: #C0FFEE;"></u>'
    msg = CfiChat::Client::Message.new html
    assert_equal :default, msg.color
  end

  def test_detect_color_priority
    html = '<i style="color: green;"></i>'
    html << '<span style="color: yellow;"></span>'
    html << '<b style="color: red;"></b>'

    msg = CfiChat::Client::Message.new html
    assert_equal 'yellow', msg.color
  end

  def test_detect_color_private
    html = '<i style="text-decoration: underline"></i>'
    html << '<span style="color: blue;"></span>'

    msg = CfiChat::Client::Message.new html
    assert_equal 'blue', msg.color
  end

  def test_detect_color_extra
    html = '<span style="background-color: #AABBCC; color: #C0FFee; font-size: 12"></span>'
    msg = CfiChat::Client::Message.new html
    assert_equal '#C0FFee', msg.color
  end

  def test_old_query
    html = '<span class="new-msg"></span>'
    msg = CfiChat::Client::Message.new html, false
    assert msg.query?
    refute msg.private?
    refute msg.new?
  end

  def test_new_query
    html = '<span class="new-msg"></span>'
    msg = CfiChat::Client::Message.new html, true
    assert msg.query?
    refute msg.private?
    assert msg.new?
  end

  def test_old_private_anybody
    html = '<i title=""></span>'
    msg = CfiChat::Client::Message.new html, false
    refute msg.query?
    assert msg.private?
    refute msg.new?
  end

  def test_new_private_anybody
    html = '<i title=""></span>'
    msg = CfiChat::Client::Message.new html, true
    refute msg.query?
    assert msg.private?
    refute msg.new?
  end

  def test_new_private
    html = '<i title="" class="new-pm"></span>'
    msg = CfiChat::Client::Message.new html, true
    refute msg.query?
    assert msg.private?
    assert msg.new?
  end

  def test_extract_time
    without_time = '<span>Service message</span>'
    with_time = 'TIME <span>user_name: foo: bar</span>'

    msg = CfiChat::Client::Message.new without_time
    assert_nil msg.time

    msg = CfiChat::Client::Message.new with_time
    assert_equal 'TIME', msg.time
  end

  def test_extract_author
    without_author = 'TIME <span>Service message: test</span>'
    with_author = 'TIME <span>user_name: foo: bar</span>'

    msg = CfiChat::Client::Message.new without_author
    assert_nil msg.author

    msg = CfiChat::Client::Message.new with_author
    assert_equal 'user_name', msg.author
  end

  def test_extract_receivers
    without_receivers = 'TIME <span>user_name: foo bar> baz</span>'
    with_receivers = 'TIME <span>user_name: second_user> TH1RD-USER>      foo bar> baz</span>'

    msg = CfiChat::Client::Message.new without_receivers
    assert_empty msg.receivers

    msg = CfiChat::Client::Message.new with_receivers
    assert_equal ['second_user', 'TH1RD-USER'], msg.receivers
  end

  def test_extract_body
    invalid = 'TIME <span>foo bar> baz</span>'
    without_receivers = 'TIME <span>user_name: foo bar> baz</span><br />second line'
    with_receivers = 'TIME <span>user_name: second_user> TH1RD-USER> foo bar> baz<br />second line</span>'

    msg = CfiChat::Client::Message.new invalid
    assert_equal 'TIME foo bar> baz', msg.body

    msg = CfiChat::Client::Message.new without_receivers
    assert_equal "foo bar> baz\nsecond line", msg.body

    msg = CfiChat::Client::Message.new with_receivers
    assert_equal "foo bar> baz\nsecond line", msg.body
  end

  def test_formatting_base
    msg = CfiChat::Client::Message.new 'hello world'
    assert_equal Paint['hello world', :default], msg.to_s

    msg = CfiChat::Client::Message.new 'hello <span style="color: #FF0000">world</span>'
    assert_equal Paint['hello world', '#FF0000'], msg.to_s
  end

  def test_bug_formatting_broke_colors
    msg = CfiChat::Client::Message.new 'hello <span style="color: #FF0000">world</span>'

    assert_equal '#FF0000', msg.color, 'before'
    msg.to_s
    assert_equal '#FF0000', msg.color, 'after'
  end

  def test_formatting_parts
    msg = CfiChat::Client::Message.new 'foo bar: baz> hello> world'
    assert_equal 'foo ' + Paint['bar: baz> hello> world', :default], msg.to_s
  end

  def test_formatting_query
    msg = CfiChat::Client::Message.new 'foo bar: <span class="new-msg">baz</span>'
    assert_equal Paint['foo ' + Paint['bar: baz', :default], :bold], msg.to_s
  end

  def test_formatting_private
    msg = CfiChat::Client::Message.new 'foo bar: <i title="">baz</span>'
    assert_equal Paint['foo ' + Paint['bar: baz', :default], :underline], msg.to_s
  end
end
