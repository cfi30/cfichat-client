require File.expand_path('../helper', __FILE__)

class TestInput < MiniTest::Test
  def setup
    @@used_instances ||= Array.new

    @client = MiniTest::Mock.new
    @client.expect :status, nil, [CfiChat::Client::OK, "Input enabled"]

    EM.run {
      EM.open_keyboard CfiChat::Client::Input, @client
      EM.next_tick { EM.stop }

      instances = ObjectSpace.each_object(CfiChat::Client::Input).to_a - @@used_instances
      @input = instances.first
      @@used_instances << @input
    }
  end

  def teardown
    @client.verify
  end

  def test_speak
    @client.expect :speak, nil, ["Hello World!"]
    @input.receive_line "Hello World!"
  end

  def test_ignore
    @client.expect :speak, nil, ["baz"]
    @input.receive_line "hello world\nfoo\ebar\ebaz"

    @client.expect :speak, nil, [""]
    @input.receive_line "foo\ebar\ebaz\e"
  end

  def test_clear
    @client.expect :status, nil, [CfiChat::Client::OK, String]
    assert_output("\e[H\e[2J") {
      @input.receive_line "/clear"
    }
  end

  def test_exit
    err = assert_raises(RuntimeError) {
      @input.receive_line "/exit!"
    }
    assert_match /evma_stop_machine/, err.message
  end

  def test_pass
    @client.expect :status, nil, [CfiChat::Client::INFO, String]
    @client.expect :status, nil, [CfiChat::Client::OK, String]
    @client.expect :room_password=, nil, ["s3cr4t p4ssw0rd"]

    STDIN.ungetc "s3cr4t p4ssw0rd\n"
    @input.receive_line "/pass"
  end

  def test_paste
    @client.expect :status, nil, [CfiChat::Client::INFO, /Enabled/]
    @input.receive_line "/paste"
    @input.receive_line "line 1"
    @input.receive_line "line 2"
    @input.receive_line "line 3"
    @input.receive_line ""
    @client.expect :status, nil, [CfiChat::Client::OK, /Disabled/]
    @client.expect :speak, nil, ["line 1\nline 2\nline 3"]
    @input.receive_line ""
  end

  def test_paste_fast_abort
    @client.expect :status, nil, [CfiChat::Client::INFO, /Enabled/]
    @input.receive_line "/paste"
    @client.expect :status, nil, [CfiChat::Client::OK, /Disabled/]
    @client.expect :speak, nil, [String.new]
    @input.receive_line ""
  end

  def test_names
    userlist = CfiChat::Client::UserList.new
    userlist.load 'rooms' => [
      {
        'label' => 'First Room',
        'users' => [
          {
            'name' => 'foo',
            'color' => 'C0FF33',
          },
          {
            'name' => 'bar',
            'color' => 'C0FF33',
            'sleeping' => 1,
          }
        ]
      },
      {
        'label' => 'Second Room',
        'users' => []
      },
      {
        'label' => 'Third Room',
        'users' => [
          {
            'name' => 'baz',
            'color' => '000000',
            'sleeping' => 1,
            'hasVoice' => true,
          },
        ]
      },
    ]

    @client.expect :user_list, userlist
    @client.expect :status, nil, [CfiChat::Client::INFO, "Room: First Room (2)"]
    @client.expect :status, nil, [CfiChat::Client::INFO, "Room: Second Room (0)"]
    @client.expect :status, nil, [CfiChat::Client::INFO, "Room: Third Room (1)"]
    @client.expect :status, nil, [CfiChat::Client::OK, "End of List"]

    user1 = Paint['foo', '#C0FF33']
    user2 = Paint['bar', '#C0FF33', :inverse]
    user3 = Paint['baz', :default, :underline, :inverse]
    assert_output "#{user1}, #{user2}\n#{user3}\n" do
      @input.receive_line "/names"
    end
  end

  def test_names_not_loaded
    @client.expect :user_list, []
    @client.expect :status, nil, [CfiChat::Client::ERR, String]

    @input.receive_line "/names"
  end

  def test_help
    @client.expect :status, nil, [CfiChat::Client::INFO, /#{@input.class}/]
    @client.expect :status, nil, [CfiChat::Client::INFO, /\/clear/]
    @client.expect :status, nil, [CfiChat::Client::INFO, /\/exit!/]
    @client.expect :status, nil, [CfiChat::Client::INFO, /\/help/]
    @client.expect :status, nil, [CfiChat::Client::INFO, /\/names/]
    @client.expect :status, nil, [CfiChat::Client::INFO, /\/pass/]
    @client.expect :status, nil, [CfiChat::Client::INFO, /\/paste/]
    @client.expect :server_url, "http://server.url/"
    @client.expect :status, nil, [CfiChat::Client::OK, /http:\/\/server.url\/\?action=help/]

    @input.receive_line "/help"
  end
end
