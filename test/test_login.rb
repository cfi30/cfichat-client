require File.expand_path('../helper', __FILE__)

class TestLogin < MiniTest::Test
  def test_login
    stub_request(:post, "http://www.server.url/")
      .with(
        :body => "login=miohomioidoskr&rid=42&cver=1.2.3&csum=123123",
        :headers => {'Cookie'=>'abc=def', 'User-Agent'=>'FooBar', 'X-Requested-With'=>'CfiChat::Client'}
      )
      .to_return(
        :body => '{"color":"C0FF33", "last_id":0, "sak":"qwfpgjluy", "time":1, "user":"Foobar", "welcome":"Hello World"}'
      )

    assert_output "s=Logged in as #{Paint['Foobar', '#C0FF33']}\nm=Hello World\nlogin\n" do
      EM.run {
        client = CfiChat::Client.new 'http://www.server.url', '1.2.3', 123123
        client.user_agent = 'FooBar'
        client.cookies = 'abc=def'
        client.login_token = 'miohomioidoskr'

        client.add_status_hook {|m| puts "s=#{m}" }
        client.add_message_hook {|m| puts "m=#{m.text}" }
        client.add_login_hook {|m| puts "login" }
        client.login 42

        EM.stop
      }
    end

    assert_requested :post, "http://www.server.url/"
  end

  def test_fake
    stub_request(:post, "http://www.server.url/")
      .to_return(
        :body => '{"color":null, "last_id":null, "sak":null, "time":null, "user":null, "welcome":null}'
      )

    assert_output "s=Logged in as #{Paint['', '#']}\nlogin\n" do
      EM.run {
        client = CfiChat::Client.new 'http://www.server.url', '1.2.3', 123123
        client.add_status_hook {|m| puts "s=#{m}" }
        client.add_message_hook {|m| puts "m=#{m.text}" }
        client.add_login_hook {|m| puts "login" }
        client.login 42

        EM.stop
      }
    end

    assert_requested :post, "http://www.server.url/"
  end

  def test_not_200
    stub_request(:post, "http://www.server.url/")
      .to_return(:status => 418)

    assert_output "Server replied with a 418 status code" do
      err = assert_raises CfiChat::Client::ClientError do
        EM.run {
          client = CfiChat::Client.new 'http://www.server.url'
          client.add_status_hook {|m| print m }
          client.login 42

          EM.stop
        }
      end

      assert_equal "Login failed", err.message
    end
  end

  def test_timeout
    stub_request(:post, "http://www.server.url/")
      .to_timeout

    err = assert_raises CfiChat::Client::ClientError do
      EM.run {
        client = CfiChat::Client.new 'http://www.server.url'
        client.login 42

        EM.stop
      }
    end

    assert_match /timeout/, err.message
  end

  def test_invalid
    stub_request(:post, "http://www.server.url/")
      .to_return(:body => '[1, 2, 3]')

    assert_output "Invalid response received (not a JSON hash)" do
      err = assert_raises CfiChat::Client::ClientError do
        EM.run {
          client = CfiChat::Client.new 'http://www.server.url'
          client.add_status_hook {|m| print m }
          client.login 42

          EM.stop
        }
      end

      assert_equal "Login failed", err.message
    end
  end

  def test_error
    stub_request(:post, "http://www.server.url/")
      .to_return(:body => '0:Error #42: Lorem ipsum')

    err = assert_raises CfiChat::Client::ServerError do
      EM.run {
        client = CfiChat::Client.new 'http://www.server.url'
        client.add_status_hook {|m| print m }
        client.login 42

        EM.stop
      }
    end

    assert_equal "Error #42: Lorem ipsum", err.message
  end

  def test_incomplete
    stub_request(:post, "http://www.server.url/")
      .to_return(:body => '{}')

    assert_output 'Invalid response received (missing keys: ["color", "last_id", "sak", "time", "user", "welcome"])' do
      err = assert_raises CfiChat::Client::ClientError do
        EM.run {
          client = CfiChat::Client.new 'http://www.server.url'
          client.add_status_hook {|m| print "#{m}" }
          client.login 42

          EM.stop
        }
      end

      assert_equal "Login failed", err.message
    end
  end
end
