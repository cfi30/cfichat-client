require File.expand_path('../helper', __FILE__)

class TestUpdate < MiniTest::Test
  def test_update
    stub_request(:post, "http://www.server.url/")
      .with(
        :body => "msg=&csum=123123&cver=1.2.3&last_id=&overview=&password=&sak=&time=&topic_crc=0&ul_crc=0&user=",
        :headers => {'Cookie'=>'abc=def', 'User-Agent'=>'FooBar', 'X-Requested-With'=>'CfiChat::Client'}
      )
      .to_return(
        :body => '{"erreur":"", "popup":"", "adminWarning":"", "invited":{}, "lockedIn":false, "topic":"", "topicHash":0, "last_id":0, "userList":"", "ul_crc":0, "messages":"", "blinkCount":0}'
      )

    assert_output "" do
      EM.run {
        client = CfiChat::Client.new 'http://www.server.url', '1.2.3', 123123
        client.user_agent = 'FooBar'
        client.cookies = 'abc=def'

        client.add_status_hook {|m| puts "s=#{m}" }
        client.add_message_hook {|m| puts "m=#{m.text}" }
        client.update

        EM.stop
      }
    end

    assert_requested :post, "http://www.server.url/"
  end

  def test_fake
    stub_request(:post, "http://www.server.url/")
      .to_return(:body => '{"erreur":null, "popup":null, "adminWarning":null, "invited":null, "lockedIn":false, "topic":null, "topicHash":null, "last_id":null, "userList":null, "ul_crc":null, "messages":null, "blinkCount":null}')

    assert_output String.new do
      EM.run {
        client = CfiChat::Client.new 'http://www.server.url', '1.2.3', 123123
        client.user_agent = 'FooBar'
        client.cookies = 'abc=def'

        client.add_status_hook {|m| puts "s=#{m}" }
        client.add_message_hook {|m| puts "m=#{m.text}" }
        client.update

        EM.stop
      }
    end
  end

  def test_not_200
    stub_request(:post, "http://www.server.url/")
      .to_return(:status => 418)

    assert_output "Server replied with a 418 status code" do
      EM.run {
        client = CfiChat::Client.new 'http://www.server.url'
        client.add_status_hook {|m| print m }
        client.update

        EM.stop
      }
    end
  end

  def test_timeout
    stub_request(:post, "http://www.server.url/")
      .to_timeout

    assert_output /timeout/ do
      EM.run {
        client = CfiChat::Client.new 'http://www.server.url'
        client.add_status_hook {|m| print m }
        client.update

        EM.stop
      }
    end
  end

  def test_invalid
    stub_request(:post, "http://www.server.url/")
      .to_return(:body => '[1, 2, 3]')

    assert_output "Invalid response received (not a JSON hash)" do
      EM.run {
        client = CfiChat::Client.new 'http://www.server.url'
        client.add_status_hook {|m| print m }
        client.update

        EM.stop
      }
    end
  end

  def test_incomplete
    stub_request(:post, "http://www.server.url/")
      .to_return(:body => '{}')

    assert_output 'Invalid response received (missing keys: ["adminWarning", "blinkCount", "erreur", "invited", "last_id", "lockedIn", "messages", "popup", "topic", "topicHash", "ul_crc", "userList"])' do
      EM.run {
        client = CfiChat::Client.new 'http://www.server.url'
        client.add_status_hook {|m| print "#{m}" }
        client.update

        EM.stop
      }
    end
  end

  def test_error_1
    stub_request(:post, "http://www.server.url/")
      .to_return(:body => '0:Error #42: Lorem ipsum')

    err = assert_raises CfiChat::Client::ServerError do
      EM.run {
        client = CfiChat::Client.new 'http://www.server.url'
        client.update

        EM.stop
      }
    end

    assert_equal "Error #42: Lorem ipsum", err.message
  end

  def test_error_2
    stub_request(:post, "http://www.server.url/")
      .to_return(:body => '{"erreur":"Error #42: Lorem ipsum", "popup":"", "adminWarning":"", "invited":{}, "lockedIn":false, "topic":"", "topicHash":0, "last_id":0, "userList":"", "ul_crc":0, "messages":"", "blinkCount":0}')

    err = assert_raises CfiChat::Client::ServerError do
      EM.run {
        client = CfiChat::Client.new 'http://www.server.url'
        client.update

        EM.stop
      }
    end

    assert_equal "Error #42: Lorem ipsum", err.message
  end

  def test_password
    stub_request(:post, "http://www.server.url/")

    EM.run {
      client = CfiChat::Client.new 'http://www.server.url'
      client.room_password = 's3cr3t passw0rd'
      client.update

      EM.stop
    }

    assert_requested :post, "http://www.server.url/", :body => "msg=&csum=&cver=&last_id=&overview=&password=s3cr3t%20passw0rd&sak=&time=&topic_crc=0&ul_crc=0&user="
  end

  def test_messages
    stub_request(:post, "http://www.server.url/")
      .to_return(:body => '{"erreur":"", "popup":"", "adminWarning":"", "invited":{}, "lockedIn":false, "topic":"", "topicHash":0, "last_id":0, "userList":"", "ul_crc":0, "messages":"<span class=\"new-msg\">message 1</span><br />message 2<br /><span class=\"new-msg\">message 3</span><br /><span class=\"new-msg\">message 4</span><br />", "blinkCount":2}')

    assert_output "m=message 1, n=true\nm=message 2, n=false\nm=message 3, n=true\nm=message 4, n=false\n" do
      EM.run {
        client = CfiChat::Client.new 'http://www.server.url'
        client.add_message_hook {|m| puts "m=#{m.text}, n=#{m.new?}" }
        client.update

        EM.stop
      }
    end
  end

  def test_popup
    stub_request(:post, "http://www.server.url/")
      .to_return(:body => '{"erreur":"", "popup":"popup-test", "adminWarning":"", "invited":{}, "lockedIn":false, "topic":"", "topicHash":0, "last_id":0, "userList":"", "ul_crc":0, "messages":"", "blinkCount":0}')

    assert_output "s=popup-test, c=yellow" do
      EM.run {
        client = CfiChat::Client.new 'http://www.server.url'
        client.add_status_hook {|m,c| print "s=#{m}, c=#{c}" }
        client.update

        EM.stop
      }
    end
  end

  def test_warning
    stub_request(:post, "http://www.server.url/")
      .to_return(:body => '{"erreur":"", "popup":"", "adminWarning":"Beat the twisted evil things.", "invited":{}, "lockedIn":false, "topic":"", "topicHash":0, "last_id":0, "userList":"", "ul_crc":0, "messages":"", "blinkCount":0}')

    assert_output "s=[WARNING] Beat the twisted evil things., c=yellow" do
      EM.run {
        client = CfiChat::Client.new 'http://www.server.url'
        client.add_status_hook {|m,c| print "s=#{m}, c=#{c}" }
        client.update

        EM.stop
      }
    end
  end

  def test_invited
    stub_request(:post, "http://www.server.url/")
      .to_return(:body => '{"erreur":"", "popup":"", "adminWarning":"", "invited":{"to":42,"name":"Another Room","by":"FOO-User"}, "lockedIn":false, "topic":"", "topicHash":0, "last_id":0, "userList":"", "ul_crc":0, "messages":"", "blinkCount":0}')

    assert_output "s=You have been invited in room 42 (Another Room) by FOO-User, c=blue" do
      EM.run {
        client = CfiChat::Client.new 'http://www.server.url'
        client.add_status_hook {|m,c| print "s=#{m}, c=#{c}" }
        client.update

        EM.stop
      }
    end
  end

  def test_locked
    client = CfiChat::Client.new 'http://www.server.url'
    client.add_status_hook {|m,c| puts "s=#{m}, c=#{c}" }

    assert_output "s=You have been locked in this room -- /join cannot be used, c=yellow\n" do
      EM.run {
        stub_request(:post, "http://www.server.url/")
          .to_return(:body => '{"erreur":"", "popup":"", "adminWarning":"", "invited":{}, "lockedIn":true, "topic":"", "topicHash":0, "last_id":0, "userList":"", "ul_crc":0, "messages":"", "blinkCount":0}')
        client.update

        EM.stop
      }
    end

    assert_output "" do
      EM.run {
        stub_request(:post, "http://www.server.url/")
          .to_return(:body => '{"erreur":"", "popup":"", "adminWarning":"", "invited":{}, "lockedIn":true, "topic":"", "topicHash":0, "last_id":0, "userList":"", "ul_crc":0, "messages":"", "blinkCount":0}')
        client.update

        EM.stop
      }
    end

    assert_output "s=You have been released, c=green\n" do
      EM.run {
        stub_request(:post, "http://www.server.url/")
          .to_return(:body => '{"erreur":"", "popup":"", "adminWarning":"", "invited":{}, "lockedIn":false, "topic":"", "topicHash":0, "last_id":0, "userList":"", "ul_crc":0, "messages":"", "blinkCount":0}')
        client.update

        EM.stop
      }
    end
  end

  def test_topic
    client = CfiChat::Client.new 'http://www.server.url'
    client.add_status_hook {|m,c| puts "s=#{m}, c=#{c}" }

    assert_output "" do
      EM.run {
        stub_request(:post, "http://www.server.url/")
          .to_return(:body => '{"erreur":"", "popup":"", "adminWarning":"", "invited":{}, "lockedIn":false, "topic":"<a href=\"http://test.url\">test</a>", "topicHash":0, "last_id":0, "userList":"", "ul_crc":0, "messages":"", "blinkCount":0}')
        client.update

        EM.stop
      }
    end

    assert_output "s=Topic: test (http://test.url), c=blue\n" do
      EM.run {
        stub_request(:post, "http://www.server.url/")
          .to_return(:body => '{"erreur":"", "popup":"", "adminWarning":"", "invited":{}, "lockedIn":false, "topic":"<a href=\"http://test.url\">test</a>", "topicHash":42, "last_id":0, "userList":"", "ul_crc":0, "messages":"", "blinkCount":0}')
        client.update

        EM.stop
      }
    end

    assert_output "" do
      EM.run {
        stub_request(:post, "http://www.server.url/")
          .to_return(:body => '{"erreur":"", "popup":"", "adminWarning":"", "invited":{}, "lockedIn":false, "topic":"", "topicHash":42, "last_id":0, "userList":"", "ul_crc":0, "messages":"", "blinkCount":0}')
        client.update

        EM.stop
      }
    end

    assert_requested :post, "http://www.server.url/", :body => "msg=&csum=&cver=&last_id=&overview=&password=&sak=&time=&topic_crc=42&ul_crc=0&user="

    assert_output "s=Topic: , c=blue\n" do
      EM.run {
        stub_request(:post, "http://www.server.url/")
          .to_return(:body => '{"erreur":"", "popup":"", "adminWarning":"", "invited":{}, "lockedIn":false, "topic":"", "topicHash":123, "last_id":0, "userList":"", "ul_crc":0, "messages":"", "blinkCount":0}')
        client.update

        EM.stop
      }
    end
  end

  def test_last_id
    client = CfiChat::Client.new 'http://www.server.url'
    client.add_status_hook {|m,c| puts "s=#{m}, c=#{c}" }

    EM.run {
      stub_request(:post, "http://www.server.url/")
        .with(:body => "msg=&csum=&cver=&last_id=&overview=&password=&sak=&time=&topic_crc=0&ul_crc=0&user=")
        .to_return(:body => '{"erreur":"", "popup":"", "adminWarning":"", "invited":{}, "lockedIn":false, "topic":"", "topicHash":0, "last_id":42, "userList":"", "ul_crc":0, "messages":"", "blinkCount":0}')
      client.update

      EM.stop
    }

    EM.run {
      stub_request(:post, "http://www.server.url/")
        .with(:body => "msg=&csum=&cver=&last_id=42&overview=&password=&sak=&time=&topic_crc=0&ul_crc=0&user=")
        .to_return(:body => '{"erreur":"", "popup":"", "adminWarning":"", "invited":{}, "lockedIn":false, "topic":"", "topicHash":0, "last_id":0, "userList":"", "ul_crc":0, "messages":"", "blinkCount":0}')
      client.update

      EM.stop
    }

    assert_requested :post, "http://www.server.url/", :body => "msg=&csum=&cver=&last_id=42&overview=&password=&sak=&time=&topic_crc=0&ul_crc=0&user="

    EM.run {
      stub_request(:post, "http://www.server.url/")
        .with(:body => "msg=&csum=&cver=&last_id=42&overview=&password=&sak=&time=&topic_crc=0&ul_crc=0&user=")
        .to_return(:body => '{"erreur":"", "popup":"", "adminWarning":"", "invited":{}, "lockedIn":false, "topic":"", "topicHash":0, "last_id":0, "userList":"", "ul_crc":0, "messages":"", "blinkCount":0}')
      client.update

      EM.stop
    }
  end

  def test_userlist
    client = CfiChat::Client.new 'http://www.server.url'

    EM.run {
      stub_request(:post, "http://www.server.url/")
        .with(:body => "msg=&csum=&cver=&last_id=&overview=&password=&sak=&time=&topic_crc=0&ul_crc=0&user=")
        .to_return(:body => '{"erreur":"", "popup":"", "adminWarning":"", "invited":{}, "lockedIn":false, "topic":"", "topicHash":0, "last_id":0, "userList":{"rooms":[{}]}, "ul_crc":1234, "messages":"", "blinkCount":0}')

      assert_equal 0, client.user_list.count
      client.update

      EM.stop
    }

    assert_equal 1, client.user_list.count

    EM.run {
      stub_request(:post, "http://www.server.url/")
        .with(:body => "msg=&csum=&cver=&last_id=&overview=&password=&sak=&time=&topic_crc=0&ul_crc=1234&user=")

      client.update

      EM.stop
    }

    assert_equal 1, client.user_list.count
    assert_requested :post, "http://www.server.url/", :body => "msg=&csum=&cver=&last_id=&overview=&password=&sak=&time=&topic_crc=0&ul_crc=1234&user="
  end
end
