require File.expand_path('../helper', __FILE__)

class TestUserList < MiniTest::Test
  def test_empty
    list = CfiChat::Client::UserList.new
    assert list.empty?
  end

  def test_load
    list = CfiChat::Client::UserList.new
    list.load 'rooms' => [{}]
    refute list.empty?
    assert_equal 1, list.count
    assert_kind_of CfiChat::Client::UserList::Room, list.first

    list.load 'rooms' => [{'users'=>[]}]
    assert_equal 1, list.count
  end

  def test_find
    list = CfiChat::Client::UserList.new
    list.load 'rooms' => [{'users'=>[{
      'uid' => '42',
      'name' => 'foo_bar',
      'status' => nil,
      'color' => 'C0FF33',
      'bullet' => 'blue',
      'group' => '1',
      'usertitle' => 'Registered User',
      'sleeping' => 1,
      'hasVoice' => 1
    }]}]
    assert_instance_of CfiChat::Client::UserList::User, list.find('foo_bar')
    assert_nil list.find('the_fox')
  end

  def test_room
    room = CfiChat::Client::UserList::Room.new \
      'rid' => '42',
      'label' => 'A Room',
      'topic' => 'Hello World',
      'forumUrl' => nil,
      'allowed' => 1,
      'muted' => 1,
      'isprivate' => 1,
      'isAdmin' => '0',
      'users' => []

    assert_equal 42, room.id
    assert_equal 'A Room', room.name
    assert_equal 'Hello World', room.topic
    assert_equal String.new, room.forum_url
    assert room.allowed?
    assert room.muted?
    assert room.private?
    refute room.op?
    assert room.empty?
    assert_equal 0, room.count
  end

  def test_user
    user = CfiChat::Client::UserList::User.new \
      'uid' => '42',
      'name' => 'foo_bar',
      'status' => nil,
      'color' => 'C0FF33',
      'bullet' => 'blue',
      'group' => '1',
      'usertitle' => 'Registered User',
      'sleeping' => 1,
      'hasVoice' => 1

    assert_equal 42, user.id
    assert_equal 'foo_bar', user.name
    assert_equal '', user.away
    assert_equal '#C0FF33', user.color
    assert_equal 'blue', user.bullet
    assert_equal 1, user.group_id
    assert_equal 'Registered User', user.group_title
    assert user.sleeping?
    refute user.voiced?
  end
end
