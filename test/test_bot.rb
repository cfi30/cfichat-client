require File.expand_path('../helper', __FILE__)

class TestBot < MiniTest::Test
  def setup
    @client = MiniTest::Mock.new
    @client.expect :status, nil, [CfiChat::Client::OK, "Bot enabled"]
    @client.expect :add_message_hook, nil

    @bot = CfiChat::Client::Bot.new @client
  end

  def test_initialize
    @client.verify
  end

  def test_execute
    def @bot.cmd_foo(*args, &block); args; end

    msg = MiniTest::Mock.new
    msg.expect :new?, true
    msg.expect :body, 'foo bar baz'

    assert_equal ['bar', 'baz'], @bot.execute(msg)
    msg.verify
  end

  def test_unknown_command_default
    msg = MiniTest::Mock.new
    msg.expect :new?, true
    msg.expect :body, 'bacon'

    @bot.execute msg
    msg.verify
  end

  def test_unknown_command_custom
    def @bot.invalid_command(name, *args, &block); [name, args]; end

    msg = MiniTest::Mock.new
    msg.expect :new?, true
    msg.expect :body, 'foo bar baz'

    assert_equal ['foo', ['bar', 'baz']], @bot.execute(msg)
    msg.verify
  end

  def test_reply_query
    def @bot.cmd_foo(*args); yield 'hello world'; end

    msg = MiniTest::Mock.new
    msg.expect :new?, true
    msg.expect :body, 'foo'
    msg.expect :query?, true
    msg.expect :author, 'user_name'

    @client.expect :speak, nil, ['user_name> hello world']

    @bot.execute msg
    msg.verify
    @client.verify
  end

  def test_reply_private
    def @bot.cmd_foo(*args); yield 'hello world'; end

    msg = MiniTest::Mock.new
    msg.expect :new?, true
    msg.expect :body, 'foo'
    msg.expect :query?, false
    msg.expect :author, 'user_name'

    @client.expect :speak, nil, ['/pm user_name> hello world']

    @bot.execute msg
    msg.verify
    @client.verify
  end

  def test_old_message
    msg = MiniTest::Mock.new
    msg.expect :new?, false

    @bot.execute msg
    msg.verify
  end

  def test_message
    def @bot.cmd_foo(*args, &block); @message.object_id; end

    msg = MiniTest::Mock.new
    msg.expect :new?, true
    msg.expect :body, 'foo bar baz'

    assert_equal msg.object_id, @bot.execute(msg)
    msg.verify
  end

  def test_fiber
    def @bot.cmd_foo(*args, &block); Fiber.current; end

    msg = MiniTest::Mock.new
    msg.expect :new?, true
    msg.expect :body, 'foo'

    refute_equal Fiber.current, @bot.execute(msg)
    msg.verify
  end
end
