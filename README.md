# cfiChat::Client

Client and API for cfiChat

## Installation

Add this line to your application's Gemfile:

    :::ruby
    gem 'cfichat-client', :git => 'https://bitbucket.org/cfi30/cfichat-client.git'

And then execute:

    $ bundle

## Usage

### Basic Client

    :::ruby
    #!/usr/bin/env ruby
    SERVER  = 'http://your.server.com/'
    ROOM    = 1

    require 'cfichat/client'

    # disable colors outside of a tty
    Paint.mode = 0 unless $stdout.tty?

    client = CfiChat::Client.new SERVER
    client.run ROOM do
      client.add_message_hook {|msg|
        # ring the bell if I have a new message
        print "\a" if msg.new?

        # use msg.text to get the message content without formatting
        puts msg
        # puts msg.text
      }
    end

### Hello World

    :::ruby
    client.run ROOM do
      # ...
      client.add_login_hook {
        client.speak "Hello World!"
      }
    end

### User Input
Type /help for a list of available commands.

    :::ruby
    client.run ROOM do
      # ...
      EM.open_keyboard CfiChat::Client::Input, client
    end

### Bot

    :::ruby
    class MyBot < CfiChat::Client::Bot
      def cmd_greet(*args)
        yield "hello #{@message.author}"
      end

      def cmd_multiply(*args)
        multiplier, multiplicand = args
        yield multiplier.to_i * multiplicand.to_i
      end

      # optional
      def invalid_command(name, *received_args)
        yield 'oops'
      end
    end

    client.run ROOM do
      # ...
      MyBot.new client
    end

### HTTP Cookies

    :::ruby
    client.cookies = "name=value; another=cookie"

### Proxy
See [available connection options](https://github.com/igrigorik/em-http-request/wiki/Issuing-Requests#wiki-available-connection--request-parameters).

    :::ruby
    client.connection_opts = {
      :proxy => {
        :host => '127.0.0.1',    # proxy address
        :port => 9000,           # proxy port
        :type => :socks5,        # default proxy mode is HTTP proxy, change to :socks5 if required

        :authorization => ['user', 'pass']  # proxy authorization header
      }
    }

### Room Password

    :::ruby
    client.room_password = "s3cr3t p@ssw0rd"

### User Agent

    :::ruby
    client.user_agent = "CustomClient/1 based on #{client.user_agent}"

### User List

    :::ruby
    client.user_list.each {|room|
      puts room.name

      room.each {|user|
        puts user.name
      }
    }

### Old Servers

    :::ruby
    VERSION = 'version_code'
    SUM     = 1234567890

    client = CfiChat::Client.new SERVER, VERSION, SUM

### Host Integration
Required on some servers.

    :::ruby
    client.login_token = "your_website_token"

### Advanced Client

    :::ruby
    #!/usr/bin/env ruby
    SERVER  = 'http://your.server.com/'
    ROOM    = 1

    require 'cfichat/client'

    EM.run {
      client = CfiChat::Client.new SERVER
      client.add_status_hook {|message, color|
        # custom status message handler
      }

      client.login ROOM

      EM.add_periodic_timer 8 do
        client.update
      end
    }

## Contributing

1. [Fork it](https://bitbucket.org/cfi30/cfichat-client/fork)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Test your changes (`bundle exec rake`)
4. Commit your changes (`git commit -am 'Add some feature'`)
5. Push to the branch (`git push origin my-new-feature`)
6. Create new Pull Request
