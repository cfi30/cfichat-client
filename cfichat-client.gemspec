# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'cfichat/client/version'

Gem::Specification.new do |spec|
  spec.name          = "cfichat-client"
  spec.version       = CfiChat::Client::VERSION
  spec.authors       = ["cfi30"]
  spec.email         = [""]
  spec.summary       = %q{Client and API for cfiChat}
  spec.homepage      = ""
  spec.license       = "LGPL-3.0"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.5"
  spec.add_development_dependency "minitest", "~> 5.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "webmock", "~> 1.18"

  spec.add_runtime_dependency "em-http-request", "~> 1.1"
  spec.add_runtime_dependency "multi_json", "~> 1.9"
  spec.add_runtime_dependency "nokogiri", "~> 1.6"
  spec.add_runtime_dependency "nokogiri-styles", "~> 0.1"
  spec.add_runtime_dependency "oj", "~> 2.9"
  spec.add_runtime_dependency "paint", "~> 0.8"
end
